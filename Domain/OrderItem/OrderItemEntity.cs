﻿using RestaurantManagement.Domain.Common;
using RestaurantManagement.Domain.Item;
using RestaurantManagement.Domain.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Domain.OrderItem
{
   public class OrderItemEntity : BaseEntity 
    {
        private OrderItemEntity() { }

        private OrderItemEntity(OrderEntity order, ItemEntity item, decimal price)
        {
            Order = order;
            Item = item;
            Price = price;
        }
        public static OrderItemEntity Create (OrderEntity order, ItemEntity item, decimal price)
        {
            return new OrderItemEntity(order, item, price);
        }
        public OrderEntity Order { get; private set; }

        public ItemEntity Item { get; private set; }
        
        public decimal Price { get; private set; }
    }
}
