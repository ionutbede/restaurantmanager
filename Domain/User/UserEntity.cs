﻿using RestaurantManagement.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Domain.User
{
   public  class UserEntity : BaseEntity
    {
        private UserEntity(string name, string password)
        {
            this.Name = name;
            this.Password = password;
           
        }
        public static UserEntity Create(string name, string password)
        {
            return new UserEntity(name, password);
        }
        public string Name
        {
            get; private set;
        }
     
        
        public string Password
        {
            get; private set;
        }

    }
}
