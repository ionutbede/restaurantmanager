﻿using RestaurantManagement.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Domain.Table
{
   public class TableEntity : BaseEntity
    {

        private TableEntity(int numberOfSeats)
        {
            this.NumberOfSeats = numberOfSeats;
        }
        public static TableEntity Create(int numberOfSeats)
        {
            return new TableEntity(numberOfSeats);
        }
        public int NumberOfSeats
        {
            get; private set;
        }
        public TableState TableState
        {
            get; private set;
        }
        public void Activate()
        {
            this.TableState = TableState.Activated;
        }
        public void Deactivate()
        {
            this.TableState = TableState.Deactivated;
        }
    }
}
