﻿using RestaurantManagement.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Domain.Item
{
   public class ItemEntity : BaseEntity 
    {
   private ItemEntity (string name, string description, decimal price)
        {
            this.Name = name;
            this.Description = description;
            this.Price = price;
        }
        public static ItemEntity Create(string name, string description, decimal price)
        {
            return new ItemEntity(name, description, price);
        }
        public string Name
        {
            get; private set;
        }
        public decimal Price
        {
            get; private set;
        }
        public string Description
        {
            get; private set;
        }
        
    }
}
