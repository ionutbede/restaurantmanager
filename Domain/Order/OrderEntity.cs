﻿using RestaurantManagement.Domain.Common;
using RestaurantManagement.Domain.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Domain.Order
{
   public class OrderEntity : BaseEntity
    {
        public List<ItemEntity> ItemEntities
        {
            get; private set;
        }
        public decimal TotalValue { get; private set; }
    }
}
