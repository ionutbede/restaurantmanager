﻿using RestaurantManagement.Persistence.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestaurantManagement.Persistence.Item;
using RestaurantManagement.Application.Interfaces.Persistence;
using RestaurantManagement.Domain.Item;

namespace RestaurantManagement.Persistence.Item
{
   public class ItemRepository : Repository<ItemEntity>, IItemRepository
    {
        public ItemRepository(DbContext databaseContext)  : base (databaseContext)
        {

        }
    }
}
