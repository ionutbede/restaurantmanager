﻿using RestaurantManagement.Domain.Item;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Persistence.Item
{
    public class ItemConfiguration
        :EntityTypeConfiguration<ItemEntity>
    {
        public ItemConfiguration()
        {
            HasKey(p => p.ID);

            Property(p => p.Price)
                .HasPrecision(8, 4);
        }
    }
}
