﻿using RestaurantManagement.Domain.Table;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Persistence.Table
{
    public class TableConfiguration
        : EntityTypeConfiguration<TableEntity>
    {
        public TableConfiguration()
        {
            HasKey(p => p.ID);
        }
    }
}
