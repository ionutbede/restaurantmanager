﻿using RestaurantManagement.Application.Interfaces.Persistence;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Persistence.Common
{
    public class Repository<T>
         : IRepository<T>
         where T : class

    {

        private readonly DbContext _databaseContext;

        public Repository(DbContext databasecontext)
        {
            _databaseContext = databasecontext;
        }

        public void Add(T entity)
        {
            _databaseContext.Set<T>().Add(entity);
        }

        public T Get(int id)
        {
           return _databaseContext.Set<T>().Find(id);
        }

        public IQueryable<T> GetAll()
        {
            return _databaseContext.Set<T>();
        }

        public void Update(T entity)
        {
            _databaseContext.Set<T>().Attach(entity);
            _databaseContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
