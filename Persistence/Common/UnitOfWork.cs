﻿using RestaurantManagement.Application.Interfaces.Persistence;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RestaurantManagement.Persistence.Common
{

       public class UnitOfWork : iUnitOfWork
        {
            private readonly DbContext _databaseContext;

            public UnitOfWork(DbContext databaseContext)
        {
            _databaseContext = databaseContext;
        }
    
        public void Save()
            {
            _databaseContext.SaveChanges();

            }
        }
    }

