namespace RestaurantManagement.Persistence.Common
{
    using RestaurantManagement.Domain.Item;
    using RestaurantManagement.Domain.Order;
    using RestaurantManagement.Domain.OrderItem;
    using RestaurantManagement.Domain.Table;
    using RestaurantManagement.Persistence.Item;
    using RestaurantManagement.Persistence.Order;
    using RestaurantManagement.Persistence.OrderItem;
    using RestaurantManagement.Persistence.Table;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class BaseDatabaseContext : DbContext
    {

        public BaseDatabaseContext()
            : base("name=BaseDatabaseContext")
        {
        }
        public virtual DbSet<Domain.Item.ItemEntity> ItemEntities { get; set; }
        public virtual DbSet<OrderEntity> OrderEntities { get; set; }
        public virtual DbSet<OrderItemEntity> OrderItemEntities { get; set; }
        public virtual DbSet<TableEntity> TableEntities { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new ItemConfiguration());
            modelBuilder.Configurations.Add(new OrderConfiguration());
            modelBuilder.Configurations.Add(new TableConfiguration());
            modelBuilder.Configurations.Add(new OrderItemConfiguration());
        }

    }
}