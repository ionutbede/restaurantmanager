﻿using RestaurantManagement.Application.Interfaces.Persistence;
using RestaurantManagement.Domain.Order;
using RestaurantManagement.Persistence.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Persistence.Order
{
    public class OrderRepository
        : Repository<OrderEntity>, IOrderRepository
    {
        public OrderRepository(DbContext databasecontext) : base(databasecontext)
        {
        }
    }
}
