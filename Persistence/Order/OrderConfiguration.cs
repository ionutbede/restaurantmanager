﻿using RestaurantManagement.Domain.Order;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Persistence.Order
{
   public class OrderConfiguration :
        EntityTypeConfiguration<OrderEntity>
    {
        public OrderConfiguration()
        {
            HasKey(p => p.ID);

            Property(p => p.TotalValue)
                .HasPrecision(8, 4);
        }
    }
}
