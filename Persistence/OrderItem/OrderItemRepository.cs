﻿using RestaurantManagement.Application.Interfaces.Persistence;
using RestaurantManagement.Domain.OrderItem;

using RestaurantManagement.Persistence.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Persistence.OrderItem
{
    public class OrderItemRepository
          : Repository<OrderItemEntity>,
          IOrderItemRepository
    {
        public OrderItemRepository(DbContext databasecontext) : base(databasecontext)
        {
        }
    }
}
