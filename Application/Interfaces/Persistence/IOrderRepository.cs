﻿using RestaurantManagement.Domain.Order;

namespace RestaurantManagement.Application.Interfaces.Persistence
{
    public interface IOrderRepository : IRepository <OrderEntity>
    { 
    }
}
