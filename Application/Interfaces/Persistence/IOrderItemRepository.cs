﻿using RestaurantManagement.Domain.OrderItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Application.Interfaces.Persistence
{
    public interface IOrderItemRepository : IRepository<OrderItemEntity>
    {
    }
}
