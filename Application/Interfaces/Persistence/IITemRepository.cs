﻿using RestaurantManagement.Domain.Item;
using RestaurantManagement.Domain.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantManagement.Application.Interfaces.Persistence
{
    public interface IItemRepository : IRepository<ItemEntity> 
    {
    }
}
